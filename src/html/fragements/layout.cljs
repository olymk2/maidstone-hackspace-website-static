(ns html.fragements.layout
  (:require [reitit.frontend.easy :as rfe]))

(defn left-links [] [{:href (rfe/href :homepage) ;"https://maidstone-hackspace.org.uk/"
                  :friendly [:span [:i.fa.fa-home.mr2] "Home"]}
                 {:href
                  (rfe/href :about)
                  ;"https://maidstone-hackspace.org.uk/about/"
                  :friendly "About"}
                 {:href "https://maidstone-hackspace.org.uk/chat/"
                  :friendly "Chat"}
                 {:href "https://maidstone-hackspace.org.uk/blog/"
                  :friendly "Blog"}
                 {:href "https://maidstone-hackspace.org.uk/wiki/"
                  :friendly "Wiki"}
                 {:href "https://maidstone-hackspace.org.uk/discuss/"
                  :friendly "Discuss"}
                 {:href "https://maidstone-hackspace.org.uk/mailing-list/"
                  :friendly "Contact"}])

(defn right-links [] [{:href "https://maidstone-hackspace.org.uk/"
                   :friendly "Sign In"}
                  {:href "https://maidstone-hackspace.org.uk/about/"
                   :class "br-pill ba pv2 ph4 b--white-20"
                   :friendly "Sign Up"}])

(defn ui-nav-bar []
  [:nav.flex.justify-between.bb.bg-navy
   [:div.link.white-70.hover-white.no-underline.pa3.w-100
    [:div.fl
     [:a.f6.link.dib.white.dim.mr3.mr3-ns {:href "/"}
           [:img#navbar-logo {:src "https://fra1.digitaloceanspaces.com/hackspace-website-bucket/static/images/logo.svg" :alt "Maidstone Hackspace Logo"}]
           [:h2.white.fr.ma2.pa1 "Maidstone Hackspace"]]]
    [:div.ma3
     (map-indexed
      (fn [idx link]
        [:a.f6.link.dib.white.dim.mr3.mr3-ns
         {:key idx :href (:href link) :class (:class link)}
         (:friendly link)])
      (left-links))]]
   [:div.flex-grow.pa3.flex.items-center.w5
    (map-indexed
     (fn [idx link]
       [:a.f6.link.dib.white.dim.mr3.mr3-ns.w5
        {:key idx :href (:href link) :class (:class link)}
        (:friendly link)])
     (right-links))]])

(defn ui-banner []
  [:article.mv4.center {:style {:width "1200px" :height "300px"}}
   [:img.db.mw-100.mh-100.br2.br--top {:src "http://placekitten.com/g/1200/300"
                                       :alt "Photo of a kitten looking menacing."}]])

(defn ui-social []
  [:div.fr
   [:a.ml2 {:href "https://github.com/maidstone-hackspace/" :target "_blank"}
    [:i.fab.fa-github]]
   [:a.ml2 {:href "https://twitter.com/mhackspace" :target "_blank"}
    [:i.fab.fa-twitter]]
   [:a.ml2 {:href "https://www.instagram.com/maidstonehackspace" :target "_blank"}
    [:i.fab.fa-instagram]]
   [:a.ml2 {:href "https://www.facebook.com/maidstonehackspace/" :target "_blank"}
    [:i.fab.fa-facebook]]
   [:a.ml2.f6.link.dim.br2.ph3.pv2.mb2.dib.white.bg-dark-blue
    {:href "https://groups.google.com/forum/#!forum/maidstone-hackspace" :role "button"} "Mailing List"]])

(defn ui-copyright []
  [:div.fl.ma1
   [:span.text-muted "© 2021 Maidstone Hackspace "]
   [:span [:a {:href "https://maidstone-hackspace.org.uk/wiki/maidstone-hackspace-constitution/"}
           "Constitution"]]])

(defn ui-footer [{:keys [year links]}]
  [:footer.pv4.ph3.ph5-m.ph6-l.bg-light-gray
   [:div.mb3
    [ui-copyright]
    [ui-social]]])

(defn ui-card []
  [:article.br2.ba.dark-gray.b--black-10.mv4.w-100.w-50-m.w-25-l.mw5.center
   [:img.db.w-100.br2.br--top {:src "http://placekitten.com/g/600/300"
                               :alt "Photo of a kitten looking menacing."}]
   [:div.pa2.ph3-ns.pb3-ns
    [:div.dt.w-100.mt1
     [:div.dtc [:h1.f5.f4-ns.mv0 "Cat"]]
     [:div.dtc.tr [:h2.f5.mv0 "$1,000"]]]
    [:p.f6.lh-copy.measure.mt2.mid-gray
     "If it fits, i sits burrow under covers. Destroy couch leave hair everywhere,\n      and touch water with paw then recoil in horror."]]])
