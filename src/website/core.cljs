(ns website.core
  (:require [reagent.core :as r]
            [reitit.frontend :as rf]
            [reitit.frontend.easy :as rfe]
            [reitit.coercion.spec :as rss]
            [reagent.dom :as dom]
            [html.fragements.layout :as c]
            [website.pages.homepage :as h]
            [website.pages.about-us :refer [ui-about-us-page]]
            [ajax.core :refer [GET POST]]
            [portal.web :as p]))

;https://websemantics.uk/articles/displaying-code-in-web-pages/
(defonce state (r/atom {:current-doc ""
                        :current-key :org-01}))

(def routes
  [["/"
    {:name :homepage
     :view h/homepage}]

   ["/about"
    {:name :about
     :view ui-about-us-page}]

   ["/item/:id"
    {:name :item
     :view h/homepage
     :parameters {:path {:id int?}
                  }}]])

(def router
  (rf/router routes {:data {:coercion rss/coercion}}))



(defn reagent-render [hiccup]
  (dom/render hiccup (js/document.getElementById "app")))

(defn fetch-document [doc]
  (GET doc #_"/documents/org-cheatsheet-01.org"
    {:handler
     (fn [document]
       (swap! state assoc :current-doc document))}))

(defn show-page [view]
   [:div 
    [view]
    ;[h/homepage]
    #_(view)
    ])

(defn layout []
  (let [] 
    (fn [] 
      [:div
       [:link {:rel "stylesheet" :href "https://unpkg.com/tachyons@4.12.0/css/tachyons.min.css"}]
       [:link {:rel "stylesheet" :href "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"}]
       [:link {:rel "stylesheet" :href "http://localhost:9500/css/custom.css"}]
       [:div [c/ui-nav-bar]]

       [:div.w-100 
        [:div.ma4.center {:style {:max-width "1140px"}}
         [show-page (or (-> @state :current-route :data :view) h/homepage)]
         #_((-> @state :current-route :data :view))]]

       ;[:div.ma4 [h/homepage]]
       [c/ui-footer]])))



(def launch-site (fn [] 
                   (rfe/start!
                    router
                    (fn [m] (swap! state assoc :current-route m))
                    ;; set to false to enable HistoryAPI
                    {:use-fragment true})
                    (reagent-render [layout])

                    ))


(defn launch-portal []
  (def portal (p/open))
  (p/tap))

(defonce single-portal (launch-portal))

;; reagent render example
(launch-site)
;  (rfe/replace-state :homepage)




(comment
  (rfe/replace-state :homepage)
  (rf/match-by-name router :homepage)
  
  )
