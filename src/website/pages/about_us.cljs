(ns website.pages.about-us)

(defn ui-about-us-page []
  [:div.container
   [:h3 "What is a hackspace/makerspace"]
   [:p "A hackspace also referred to as a maker space is a community-operated, workspace where people with common interests, often in computers, machining, technology, science, digital art or electronic art, can meet, socialise and collaborate."]
   [:p "Most people do not have the space knowledge or finance required to create things at home, hackspaces get around this by pooling together for the bigger tools sharing of knowledge and helping out each other to create & build."]
   [:h3 "Where we are based"] "
We currently have a room at Maidstone Community Support Centre."
   [:br]
   [:iframe#gmap_canvas {:src "https://maps.google.com/maps?q=maidstone%20hackspace&t=&z=15&ie=UTF8&iwloc=&output=embed" :scrolling "no" :marginheight "0" :marginwidth "0" :width "550" :height "350" :frameborder "0"}]
   [:p "Maidstone Community Support Centre" [:br] "
22 Marsham Street"
    [:br] " Maidstone"
    [:br] " ME14 1HG"]
   [:h3 "Contact Us"]
   [:p "If you have questions you would like answered you can ask in "
    [:a {:href "https://app.element.io/#/room/#maidstone-hackspace:matrix.org"} "chatroom"] ", bare in mind everyone will likely be busy so be patient or preferably use the
  " [:a {:href "https://groups.google.com/forum/#!forum/maidstone-hackspace"} "mailing list"] " or "
    [:a {:href "/contact/"} "contact form"] "."]
   [:h3 "When do we meet?"]
   [:p "We would normally hold open evenings "
    [:strong "every wednesday"] " from "
    [:strong "5:30pm"] " till "
    [:strong " 8:00pm "] "
where members and non-members can visit and hang out, work on projects, socialise, and collaborate.
This has been suspended during the COVID outbreak.
  " [:a {:href "/about/"} "Learn more"] "."] "

We have a calendar you can subscribe to which has our meetups we try to keep this up to date.
" [:a {:href "https://calendar.google.com/calendar/embed?src=contact@maidstone-hackspace.org.uk&ctz=America/New_York&pli=1"} "Shared Calendar"]])
